package handler

import (
	"../model"
    "net/http"
    "github.com/labstack/echo"
    mgo "gopkg.in/mgo.v2"
    "gopkg.in/mgo.v2/bson"
    "github.com/labstack/gommon/log"
    "math/rand"
    "time"
)

func (h *Handler) CreateGame(c echo.Context) (err error) {
    g := new(model.Game)
    if err = c.Bind(g); err != nil {
        return
    }
    
    log.Printf(g.Players[0].Name)
    log.Printf(g.Name)

    // Validation
    if g.Players[0].Name == "" {
        return &echo.HTTPError{Code: http.StatusBadRequest, Message: "Invalid Player1 Name"}
    }
    if g.Name == "" {
        return &echo.HTTPError{Code: http.StatusBadRequest, Message: "Invalid Game Name"}
    }

    var games []model.Game
    db := h.DB.Clone()
    state := "waitingforplayerstojoin"
    if err = db.DB("bombs_and_arrows_server").C("games").
        Find(bson.M{"state": state}).
        All(&games);err != nil {
        return
    }
    defer db.Close()

    for i := 0; i < len(games); i++ {
        if games[i].Name == g.Name {
            return &echo.HTTPError{Code: http.StatusBadRequest, Message: "Game Name Already Exists"}
        }
    }

    
    g.ID = bson.NewObjectId()
    g.Players[0].ID = bson.NewObjectId()
    g.Players[0].IsTurn = false;
    g.Players[0].IsWinner = false;
    g.Players[0].Tower = model.Tower{ Coordinate: model.Coordinate{4,6},
                                   IsDestroyed: false }
    g.Players[0].Index = 0;

    g.Players[1].IsTurn = false;
    g.Players[1].IsWinner = false;
    g.Players[1].Tower = model.Tower{ Coordinate: model.Coordinate{4,0},
                                    IsDestroyed: false }
    g.Players[1].Index = 1;

    g.State = "waitingforplayerstojoin"
    var tiles [9][7]model.Tile 

    var bushCoordinates []model.Coordinate 
    bushCoordinates = append(bushCoordinates, model.Coordinate{1,0}) 
    bushCoordinates = append(bushCoordinates, model.Coordinate{1,1}) 
    bushCoordinates = append(bushCoordinates, model.Coordinate{1,2}) 
    bushCoordinates = append(bushCoordinates, model.Coordinate{1,4}) 
    bushCoordinates = append(bushCoordinates, model.Coordinate{1,5}) 
    bushCoordinates = append(bushCoordinates, model.Coordinate{1,6}) 
    bushCoordinates = append(bushCoordinates, model.Coordinate{3,1}) 
    bushCoordinates = append(bushCoordinates, model.Coordinate{3,2}) 
    bushCoordinates = append(bushCoordinates, model.Coordinate{3,4}) 
    bushCoordinates = append(bushCoordinates, model.Coordinate{3,5}) 
    bushCoordinates = append(bushCoordinates, model.Coordinate{4,1}) 
    bushCoordinates = append(bushCoordinates, model.Coordinate{4,5}) 
    bushCoordinates = append(bushCoordinates, model.Coordinate{5,1}) 
    bushCoordinates = append(bushCoordinates, model.Coordinate{5,2}) 
    bushCoordinates = append(bushCoordinates, model.Coordinate{5,4}) 
    bushCoordinates = append(bushCoordinates, model.Coordinate{5,5}) 
    bushCoordinates = append(bushCoordinates, model.Coordinate{7,0}) 
    bushCoordinates = append(bushCoordinates, model.Coordinate{7,1}) 
    bushCoordinates = append(bushCoordinates, model.Coordinate{7,2}) 
    bushCoordinates = append(bushCoordinates, model.Coordinate{7,4}) 
    bushCoordinates = append(bushCoordinates, model.Coordinate{7,5}) 
    bushCoordinates = append(bushCoordinates, model.Coordinate{7,6}) 

    for i := 0; i < 9; i++ {
        for j := 0; j < 7; j++ {
            var isBush = false
            for k := 0; k < len(bushCoordinates); k++ {
                if bushCoordinates[k].X == i && bushCoordinates[k].Y == j {
                    isBush = true;
                }
            }
            var tile model.Tile
            tile.Coordinate = model.Coordinate{i,j}
            if isBush {
                tile.TileType = "bush"
            } else {
                tile.TileType = "blank"
            }
            tiles[i][j] = tile
        }
    }

    g.Tiles = tiles
    var redBombCoordinate model.Coordinate
    redBombCoordinate.X = 2
    redBombCoordinate.Y = 3
    g.Bombs[0] = model.Bomb{ Coordinate: redBombCoordinate,
                              Color: "red" }

    var greenBombCoordinate model.Coordinate
    greenBombCoordinate.X = 4
    greenBombCoordinate.Y = 3
    g.Bombs[1] = model.Bomb{ Coordinate: greenBombCoordinate,
                              Color: "green"}

    var blueBombCoordinate model.Coordinate
    blueBombCoordinate.X = 6
    blueBombCoordinate.Y = 3
    g.Bombs[2] = model.Bomb{ Coordinate: blueBombCoordinate,
                            Color: "blue" }
    
    var availableMoves [3]model.AvailableMove
    g.Players[0].AvailableMoves = availableMoves
    g.Players[1].AvailableMoves = availableMoves
    var appliedMoves [3]model.AppliedMove
    g.Players[0].AppliedMoves = appliedMoves                      
    g.Players[1].AppliedMoves = appliedMoves                      

    db = h.DB.Clone()
    defer db.Close()
    if err = db.DB("bombs_and_arrows_server").C("games").Insert(*g); err != nil {
        return
    }
    return c.JSON(http.StatusCreated, g)
}

func (h *Handler) FetchGame(c echo.Context) (err error) {
    gameid := c.Param("gameid")
    playerid := c.Param("playerid")

    // Retrieve game from database
    game := new(model.Game)
    db := h.DB.Clone()
    if err = db.DB("bombs_and_arrows_server").C("games").
        Find(bson.M{"_id": bson.ObjectIdHex(gameid)}).One(game);err != nil {
        return
    }
    defer db.Close()

    if game.Players[0].ID == bson.ObjectIdHex(playerid) {
        clearPlayer2Data(game)
    } else if game.Players[1].ID == bson.ObjectIdHex(playerid) {
        clearPlayer1Data(game)
    } else {
        return &echo.HTTPError{Code: http.StatusBadRequest, Message: "Invalid Player ID"}
    }

    return c.JSON(http.StatusOK, game)    
}

func (h *Handler) FetchGames(c echo.Context) (err error) {
    // Retrieve games from database
    var games []model.Game
    db := h.DB.Clone()
    state := "waitingforplayerstojoin"
    if err = db.DB("bombs_and_arrows_server").C("games").
        Find(bson.M{"state": state}).
		All(&games);err != nil {
        return
    }
    defer db.Close()

    joinableGames := []*model.JoinableGame{}

    for i := 0; i < len(games); i++ {
    	var joinableGame model.JoinableGame
    	joinableGame.ID = games[i].ID
    	joinableGame.GameName = games[i].Name
    	joinableGames = append(joinableGames, &joinableGame)
    }

    return c.JSON(http.StatusOK, joinableGames)    
}

func (h *Handler) JoinGame(c echo.Context) (err error) {
    player2 := new(model.Player)
    if err = c.Bind(player2); err != nil {
        return
    }

    // Validation
    if player2.Name == "" {
        return &echo.HTTPError{Code: http.StatusBadRequest, Message: "Invalid Player2 Name"}
    }

    gameid := c.Param("id")
    game := new(model.Game)
    db := h.DB.Clone()
    if err = db.DB("bombs_and_arrows_server").C("games").
        Find(bson.M{"_id": bson.ObjectIdHex(gameid)}).One(game);err != nil {
        return
    }
    defer db.Close()

    player1 := game.Players[0]

    player2.ID = bson.NewObjectId();
    player2.IsWinner = false;
    player2.Index = 1;
    player2.Tower = model.Tower{ Coordinate: model.Coordinate{4,0},
                                 IsDestroyed: false}

    var randomTurn int
    rand.Seed(time.Now().UnixNano())
    randomTurn = rand.Intn(2)
    if randomTurn == 1 {
        player1.IsTurn = true;
        player2.IsTurn = false;
    } else {
        player1.IsTurn = false;
        player2.IsTurn = true;
    }

    player1AvailableMoves := make([]model.AvailableMove, 3)
    player1AvailableMoves[0] = model.AvailableMove{0, moveStringFromInt(rand.Intn(4))}
    player1AvailableMoves[1] = model.AvailableMove{1, moveStringFromInt(rand.Intn(4))}
    player1AvailableMoves[2] = model.AvailableMove{2, moveStringFromInt(rand.Intn(4))}
    player2AvailableMoves := make([]model.AvailableMove, 3)
    player2AvailableMoves[0] = model.AvailableMove{0, moveStringFromInt(rand.Intn(4))}
    player2AvailableMoves[1] = model.AvailableMove{1, moveStringFromInt(rand.Intn(4))}
    player2AvailableMoves[2] = model.AvailableMove{2, moveStringFromInt(rand.Intn(4))}
    
    player1.AvailableMoves[0] = player1AvailableMoves[0]
    player1.AvailableMoves[1] = player1AvailableMoves[1]
    player1.AvailableMoves[2] = player1AvailableMoves[2]
    player2.AvailableMoves[0] = player2AvailableMoves[0]
    player2.AvailableMoves[1] = player2AvailableMoves[1]
    player2.AvailableMoves[2] = player2AvailableMoves[2]

    players := make([]model.Player, 2)
    players[0] = player1
    players[1] = *player2
    
    db = h.DB.Clone()
    game = new(model.Game)
    change := mgo.Change{ 
        Update: bson.M{"$set": bson.M{"players": players,
                                      "state": "turns"}}, 
        ReturnNew: true,
    }

    if _, err = db.DB("bombs_and_arrows_server").C("games").
        Find(bson.M{"_id": bson.ObjectIdHex(gameid)}).Apply(change, game);err != nil {
        return
    }
    defer db.Close()

    clearPlayer1Data(game)

    return c.JSON(http.StatusOK, game)
}

func (h *Handler) SubmitMoves(c echo.Context) (err error) {
    movesToApply := new([3]model.AppliedMove)
    if err = c.Bind(movesToApply); err != nil {
        return
    }
    gameID := c.Param("gameid")
    playerID := c.Param("playerid")
    var submitter int

    // Retrieve game from database
    game := new(model.Game)
    db := h.DB.Clone()
    if err = db.DB("bombs_and_arrows_server").C("games").
        Find(bson.M{"_id": bson.ObjectIdHex(gameID)}).One(game);err != nil {
        return
    }
    defer db.Close()

    if game.Players[0].ID == bson.ObjectIdHex(playerID) {
        if(!game.Players[0].IsTurn) {
            return &echo.HTTPError{Code: http.StatusBadRequest, Message: "Not Your Turn"}
        }
    } else if game.Players[1].ID == bson.ObjectIdHex(playerID) {
        if(!game.Players[1].IsTurn) {
            return &echo.HTTPError{Code: http.StatusBadRequest, Message: "Not Your Turn"}
        }
    } else {
        return &echo.HTTPError{Code: http.StatusBadRequest, Message: "Invalid Player Name For Submit Moves"}
    }

    if game.State == "gamefinished" {
        return &echo.HTTPError{Code: http.StatusBadRequest, Message: "Game Over"}
    }

    var nextState string

    var player1AvailableMoves [3]model.AvailableMove
    var player2AvailableMoves [3]model.AvailableMove

    var player1AppliedMoves [3]model.AppliedMove
    var player2AppliedMoves [3]model.AppliedMove

    var redBomb = game.Bombs[0]
    var greenBomb = game.Bombs[1]
    var blueBomb = game.Bombs[2]

    player1AvailableMoves = game.Players[0].AvailableMoves
    player2AvailableMoves = game.Players[1].AvailableMoves

    if game.Players[0].IsTurn {
        player1AppliedMoves = *movesToApply
        rand.Seed(time.Now().UnixNano())
        player1AvailableMoves[0] = model.AvailableMove{0, ""}
        player1AvailableMoves[1] = model.AvailableMove{1, ""}
        player1AvailableMoves[2] = model.AvailableMove{2, ""}
        player2AvailableMoves[0] = model.AvailableMove{0, moveStringFromInt(rand.Intn(4))}
        player2AvailableMoves[1] = model.AvailableMove{1, moveStringFromInt(rand.Intn(4))}
        player2AvailableMoves[2] = model.AvailableMove{2, moveStringFromInt(rand.Intn(4))}
    } else if game.Players[1].IsTurn {
        player2AppliedMoves = *movesToApply
        rand.Seed(time.Now().UnixNano())
        player1AvailableMoves[0] = model.AvailableMove{0, moveStringFromInt(rand.Intn(4))}
        player1AvailableMoves[1] = model.AvailableMove{1, moveStringFromInt(rand.Intn(4))}
        player1AvailableMoves[2] = model.AvailableMove{2, moveStringFromInt(rand.Intn(4))}
        player2AvailableMoves[0] = model.AvailableMove{0, ""}
        player2AvailableMoves[1] = model.AvailableMove{1, ""}
        player2AvailableMoves[2] = model.AvailableMove{2, ""}
    } 

    player1 := new(model.Player)
    player2 := new(model.Player)

    nextState = game.State

    if game.State == "turns"{
        if(game.Players[0].IsTurn == true) {
            player1.IsWinner, player2.IsWinner, redBomb, greenBomb, blueBomb, nextState = applyMoves(1, game, redBomb, greenBomb, blueBomb, player1AppliedMoves, nextState)
            player1.IsTurn = false;
            player2.IsTurn = true;

        } else if(game.Players[1].IsTurn == true) {
            player1.IsWinner, player2.IsWinner, redBomb, greenBomb, blueBomb, nextState = applyMoves(2, game, redBomb, greenBomb, blueBomb, player2AppliedMoves, nextState)
            player1.IsTurn = true;
            player2.IsTurn = false;
        }
    }

    player1.ID = game.Players[0].ID
    player1.Name = game.Players[0].Name
    player1.AvailableMoves = player1AvailableMoves
    player1.AppliedMoves = player1AppliedMoves
    player1.Tower = game.Players[0].Tower

    player2.ID = game.Players[1].ID
    player2.Name = game.Players[1].Name
    player2.AvailableMoves = player2AvailableMoves
    player2.AppliedMoves = player2AppliedMoves
    player2.Tower = game.Players[1].Tower

    players := make([]model.Player, 2)
    players[0] = *player1
    players[1] = *player2

    bombs := make([]model.Bomb, 3)
    bombs[0] = redBomb
    bombs[1] = greenBomb
    bombs[2] = blueBomb

    db = h.DB.Clone()
    game = new(model.Game)
    change := mgo.Change{ 
        Update: bson.M{"$set": bson.M{"state": nextState, 
                                      "players": players,
                                      "bombs": bombs}}, 
        ReturnNew: true,
    }

    if _, err = db.DB("bombs_and_arrows_server").C("games").
        Find(bson.M{"_id": bson.ObjectIdHex(gameID)}).Apply(change, game);err != nil {
        return
    }
    defer db.Close()

    if(submitter == 1) {
        clearPlayer2Data(game)
    } else {
        clearPlayer1Data(game)
    }

    return c.JSON(http.StatusOK, game)
}

func moveStringFromInt(index int) string {
    if index == 0 {
        return "forward"
    } else if index == 1 {
        return "right"
    } else if index == 2 {
        return "left"
    } else {
        return "backward"
    }
}

func applyMoves(submitter int, game *model.Game, redBomb model.Bomb, greenBomb model.Bomb, blueBomb model.Bomb, movesToApply [3]model.AppliedMove, nextState string) (bool, bool, model.Bomb, model.Bomb, model.Bomb, string) {
    var state = nextState
    var player1IsWinner = game.Players[0].IsWinner
    var player2IsWinner = game.Players[1].IsWinner

    for i := 0; i < 3; i++ {
        if submitter == 1 {
            if movesToApply[i].BombColor == "red" {
                for j := 0; j < 3; j++ {
                    if movesToApply[i].AvailableMoveId == game.Players[0].AvailableMoves[j].Index {
                        player1IsWinner, player2IsWinner, redBomb, state = applyMove(game, game.Players[0].AvailableMoves[j].Move, redBomb, redBomb, greenBomb, blueBomb, state)
                    }
                }
            } else if movesToApply[i].BombColor == "green" {
                for j := 0; j < 3; j++ {
                    if movesToApply[i].AvailableMoveId == game.Players[0].AvailableMoves[j].Index {
                        player1IsWinner, player2IsWinner, greenBomb, state = applyMove(game, game.Players[0].AvailableMoves[j].Move, greenBomb, redBomb, greenBomb, blueBomb, state)
                    }
                }
            } else if movesToApply[i].BombColor == "blue" {
                for j := 0; j < 3; j++ {
                    if movesToApply[i].AvailableMoveId == game.Players[0].AvailableMoves[j].Index {
                        player1IsWinner, player2IsWinner, blueBomb, state = applyMove(game, game.Players[0].AvailableMoves[j].Move, blueBomb, redBomb, greenBomb, blueBomb, state)
                    }
                }
            }
        } else {
            if movesToApply[i].BombColor == "red" {
                for j := 0; j < 3; j++ {
                    if movesToApply[i].AvailableMoveId == game.Players[1].AvailableMoves[j].Index {
                        player1IsWinner, player2IsWinner, redBomb, state = applyMove(game, game.Players[1].AvailableMoves[j].Move, redBomb, redBomb, greenBomb, blueBomb, state)
                    }
                }
            } else if movesToApply[i].BombColor == "green" {
                for j := 0; j < 3; j++ {
                    if movesToApply[i].AvailableMoveId == game.Players[1].AvailableMoves[j].Index {
                        player1IsWinner, player2IsWinner, greenBomb, state = applyMove(game, game.Players[1].AvailableMoves[j].Move, greenBomb, redBomb, greenBomb, blueBomb, state)
                    }
                }
            } else if movesToApply[i].BombColor == "blue" {
                for j := 0; j < 3; j++ {
                    if movesToApply[i].AvailableMoveId == game.Players[1].AvailableMoves[j].Index {
                        player1IsWinner, player2IsWinner, blueBomb, state = applyMove(game, game.Players[1].AvailableMoves[j].Move, blueBomb, redBomb, greenBomb, blueBomb, state)
                    }
                }
            }
        }
    }

    return player1IsWinner, player2IsWinner, redBomb, greenBomb, blueBomb, state
}

func applyMove(game *model.Game, move string, bomb model.Bomb, redBomb model.Bomb, greenBomb model.Bomb, blueBomb model.Bomb, state string) (bool, bool, model.Bomb, string) {
    var targetCoordinate model.Coordinate
    var player1IsWinner = game.Players[0].IsWinner;
    var player2IsWinner = game.Players[1].IsWinner;
    log.Printf("%s", move)
    if(move == "forward") {
        targetCoordinate.X = bomb.Coordinate.X;
        targetCoordinate.Y = bomb.Coordinate.Y - 1;
    } else if(move == "backward") {
        targetCoordinate.X = bomb.Coordinate.X;
        targetCoordinate.Y = bomb.Coordinate.Y + 1;
    } else if(move == "left") {
        targetCoordinate.X = bomb.Coordinate.X - 1;
        targetCoordinate.Y = bomb.Coordinate.Y;
    } else if(move == "right") {
        targetCoordinate.X = bomb.Coordinate.X + 1;
        targetCoordinate.Y = bomb.Coordinate.Y;
    } 

    var bombReturn = bomb
    var stateReturn = state

    if targetCoordinate.X == game.Players[0].Tower.Coordinate.X && targetCoordinate.Y == game.Players[0].Tower.Coordinate.Y {
        stateReturn = "gamefinished"
        player2IsWinner = true;
        bombReturn.Coordinate.X = targetCoordinate.X
        bombReturn.Coordinate.Y = targetCoordinate.Y
    } else if targetCoordinate.X == game.Players[1].Tower.Coordinate.X && targetCoordinate.Y == game.Players[1].Tower.Coordinate.Y {
        stateReturn = "gamefinished"
        player1IsWinner = true;
        bombReturn.Coordinate.X = targetCoordinate.X
        bombReturn.Coordinate.Y = targetCoordinate.Y
    } else if targetCoordinate.X >= 0 && targetCoordinate.X < 9 && targetCoordinate.Y >= 0 && targetCoordinate.Y < 7 {
        if game.Tiles[targetCoordinate.X][targetCoordinate.Y].TileType == "blank" && 
         !(redBomb.Coordinate.X == targetCoordinate.X && redBomb.Coordinate.Y == targetCoordinate.Y) && 
         !(greenBomb.Coordinate.X == targetCoordinate.X && greenBomb.Coordinate.Y == targetCoordinate.Y) && 
         !(blueBomb.Coordinate.X == targetCoordinate.X && blueBomb.Coordinate.Y == targetCoordinate.Y) {
            bombReturn.Coordinate.X = targetCoordinate.X
            bombReturn.Coordinate.Y = targetCoordinate.Y
        }
    }

    return player1IsWinner, player2IsWinner, bombReturn, stateReturn
}

func clearPlayer1Data(game *model.Game) {
    game.Players[0].ID = bson.ObjectIdHex("000000000000000000000000")
    game.Players[0].AvailableMoves = [3]model.AvailableMove{}
    game.Players[0].AppliedMoves = [3]model.AppliedMove{}
}

func clearPlayer2Data(game *model.Game) {
    game.Players[1].ID = bson.ObjectIdHex("000000000000000000000000")
    game.Players[1].AvailableMoves = [3]model.AvailableMove{}
    game.Players[1].AppliedMoves = [3]model.AppliedMove{}
}
