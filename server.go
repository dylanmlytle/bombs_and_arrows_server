package main

import (
	"./handler"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/labstack/gommon/log"
	mgo "gopkg.in/mgo.v2"
)

func main() {
	e := echo.New()
	e.Logger.SetLevel(log.ERROR)
	e.Use(middleware.Logger())
	// e.Use(middleware.JWTWithConfig(middleware.JWTConfig{
	// 	SigningKey: []byte(handler.Key),
	// 	Skipper: func(c echo.Context) bool {
	// 		// Skip authentication for and signup login requests
	// 		if c.Path() == "/login" || c.Path() == "/signup" ||
	// 		   c.Path() == "/games" || c.Path() == "/game" {
	// 			return true
	// 		}
	// 		return false
	// 	},
	// }))

	// Database connection
	db, err := mgo.Dial("127.0.0.1")
	if err != nil {
		e.Logger.Fatal(err)
	}

	// Create indices
	if err = db.Copy().DB("bombs_and_arrows_server").C("games").EnsureIndex(mgo.Index{
		Key: []string{"_id"},
		Unique: false,
	}); err != nil {
		log.Fatal(err)
	}

	// Initialize handler
	h := &handler.Handler{DB: db}

	// Routes
	e.POST("/games", h.CreateGame)
	e.GET("/games/:gameid/:playerid", h.FetchGame)
	e.POST("/games/:id/join", h.JoinGame)
	e.POST("/games/:gameid/submitmoves/:playerid", h.SubmitMoves)
	e.GET("/games", h.FetchGames)

	// Start server
	e.Logger.Fatal(e.Start(":6895"))
}
