package model

type (
    AppliedMove struct {
        Index           int    `json:"index" bson:"index"`  
        BombColor       string `json:"bombcolor" bson:"bombcolor"`   
        AvailableMoveId int    `json:"availablemoveid" bson:"availablemoveid"`
    }
)
