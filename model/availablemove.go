package model

type (
    AvailableMove struct {
        Index int    `json:"index" bson:"index"`  
        Move  string `json:"move" bson:"move"`   
    }
)
