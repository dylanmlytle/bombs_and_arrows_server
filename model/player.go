package model

import "gopkg.in/mgo.v2/bson"

type (
    Player struct {
    	ID             bson.ObjectId    `json:"id" bson:"_id,omitempty"`
        Index          int              `json:"index" bson:"index"`
        Name           string           `json:"name" bson:"name"`
        AvailableMoves [3]AvailableMove `json:"availablemoves" bson:"availablemoves"`
        AppliedMoves   [3]AppliedMove   `json:"appliedmoves" bson:"appliedmoves"`
        Tower          Tower            `json:"tower" bson:"tower"`
        IsTurn         bool             `json:"isturn" bson:"isturn"`
        IsWinner       bool             `json:"iswinner" bson:"iswinner"`
    }
)