package model

type (
    Tile struct {
        Coordinate Coordinate `json:"coordinate" bson:"coordinate"`   
        TileType   string     `json:"tiletype" bson:"tiletype"`
    }
)
