package model

import "gopkg.in/mgo.v2/bson"

type (
    JoinableGame struct {
        ID       bson.ObjectId `json:"id" bson:"_id,omitempty"`
        GameName string        `json:"gamename" bson:"gamename"`
    }
)