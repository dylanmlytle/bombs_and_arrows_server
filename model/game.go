package model

import "gopkg.in/mgo.v2/bson"

type (
    Game struct {
        ID                    bson.ObjectId    `json:"id" bson:"_id,omitempty"`
        Name                  string           `json:"name" bson:"name"`
        State                 string           `json:"state" bson:"state"`
        Players               [2]Player        `json:"players" bson:"players"`
        Tiles                 [9][7]Tile       `json:"tiles" bson:"tiles"`
        Bombs                 [3]Bomb          `json:"bombs" bson:"bombs"`
    }
)