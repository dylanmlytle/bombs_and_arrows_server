package model

type (
    Coordinate struct {
        X int `json:"x" bson:"x"`   
        Y int `json:"y" bson:"y"`
    }
)
