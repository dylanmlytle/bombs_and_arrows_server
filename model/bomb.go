package model

type (
    Bomb struct {
        Coordinate Coordinate `json:"coordinate" bson:"coordinate"`  
        Color      string     `json:"color" bson:"color"`      
    }
)