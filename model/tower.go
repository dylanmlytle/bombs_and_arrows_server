package model

type (
    Tower struct {
        Coordinate  Coordinate `json:"coordinate" bson:"coordinate"`   
        IsDestroyed bool       `json:"isdestroyed" bson:"isdestroyed"`
    }
)
